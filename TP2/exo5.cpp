#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size() <= 1) {
        return;
    }

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    int index = 0;
    for (int i = 0; i < first.size(); i++) {
        first[i] = origin[index++];
    }
    for (int i = 0; i < second.size(); i++) {
        second[i] = origin[index++];
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);

    // merge
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    int cursorFirst = 0;
    int cursorSecond = 0;
    int index = 0;

    while (cursorFirst < first.size() && cursorSecond < second.size()) {
        if (first[cursorFirst] < second[cursorSecond]) {
            result[index++] = first[cursorFirst++];
        }
        else {
            result[index++] = second[cursorSecond++];
        }
    }
    if (cursorFirst < first.size()){
        for (int i = cursorFirst; i < first.size(); i++) {
            result[index++] = first[i];
        }
    }
    else if (cursorSecond < second.size()){
        for (int i = cursorSecond; i < second.size(); i++) {
            result[index++] = second[i];
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
