#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int longueur;
    // your code
    int capacite;
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == nullptr)
        return true;
    else
        return false;
}

// ajoute une valeur à la fin de la structure (alloue de la mémoire en plus si besoin)
void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveauNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;
    nouveauNoeud->suivant = nullptr;

    Noeud* dernierElement = liste->premier;
    if (est_vide(liste))
    {
        liste->premier = nouveauNoeud;
    }
    else
    {
        while (dernierElement->suivant != nullptr)
        {
            dernierElement = dernierElement->suivant;
        }
        dernierElement->suivant = nouveauNoeud;
    }
}

void affiche(const Liste* liste)
{
    Noeud* dernierElement = liste->premier;
    while (dernierElement != nullptr)
    {
        printf("%d ", dernierElement->donnee);
        dernierElement = dernierElement->suivant;
    }
    printf("\n");
}

// retourne le n ième entier de la structure
int recupere(const Liste* liste, int n)
{
    Noeud* dernierElement = liste->premier;
    for (int i = 0; i < n; i++)
    {
        if (dernierElement != nullptr)
        {
            dernierElement = dernierElement->suivant;
        }
        else
        {
            printf("ERREUR: index out of bound");
            return -1;
        }
    }
    return dernierElement->donnee;
}

// retourne l'index de la valeur recherchée dans la structure OU -1 s'il n'existe pas
int cherche(const Liste* liste, int valeur)
{
    int index = 0;
    Noeud* dernierElement = liste->premier;
    while (dernierElement != nullptr)
    {
        if (dernierElement->donnee == valeur)
            return index;
        index++;
        dernierElement = dernierElement->suivant;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* dernierElement = liste->premier;
    for (int i = 0; i < n; i++)
    {
        if (dernierElement != nullptr)
        {
            dernierElement = dernierElement->suivant;
        }
        else
        {
            printf("ERREUR: index out of bound");
        }
    }
    dernierElement->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->longueur > tableau->capacite)
    {
        tableau->capacite++;
        int* newTab = new int[tableau->capacite];

        for (int i = 0; i < tableau->capacite; i++)
        {
            newTab[i] = tableau->donnees[i];
        }

        newTab[tableau->capacite] = valeur;
        tableau->donnees = newTab;

        tableau->longueur++;
    }
    else
    {
        tableau->donnees[tableau->longueur] = valeur;
        tableau->longueur++;
    }
    //printf("valeur: %i, Tl: %i, Tc: %i\n", valeur, tableau->longueur, tableau->capacite);
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees = new int[capacite];
    tableau->capacite = capacite;
    tableau->longueur = 0;
}

bool est_vide(const DynaTableau* liste)
{
    if (liste->longueur == 0)
        return true;
    return false;
}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i < tableau->longueur; i++)
    {
        printf("%i ", tableau->donnees[i]);
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (n < tableau->longueur)
        return tableau->donnees[n];
    else
    {
        printf("ERREUR: index out of bound");
        return -1;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i = 0; i < tableau->longueur; i++)
    {
        if (tableau->donnees[i] == valeur)
            return i;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (n < tableau->longueur)
        tableau->donnees[n] = valeur;
    else
    {
        printf("ERREUR: index out of bound");
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud* nouveauNoeud = new Noeud;
    nouveauNoeud->donnee = valeur;

    if (est_vide(liste))
    {
        nouveauNoeud->suivant = nullptr;
    }
    else
    {
        nouveauNoeud->suivant = liste->premier;
    }
    liste->premier = nouveauNoeud;
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud* dernierElement = liste->premier;
    while (dernierElement->suivant != nullptr)
    {
        dernierElement = dernierElement->suivant;
    }
    int valeur = dernierElement->donnee;
    if (liste->premier->suivant == nullptr)
        liste->premier = nullptr;
    else
        dernierElement = nullptr;
    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud* dernierElement = liste->premier;
    while (dernierElement->suivant != nullptr)
    {
        dernierElement = dernierElement->suivant;
    }
    int valeur = dernierElement->donnee;
    if (liste->premier->suivant == nullptr)
        liste->premier = nullptr;
    else
        dernierElement = nullptr;
    return valeur;
}

// FILE : AJOUTE AU DEBUT, RETIRE A LA FIN
// PILE : AJOUTE A LA FIN, RETIRE A LA FIN

int main()
{
    Liste liste;
    initialise(&liste);

    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    affiche(&liste);

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
